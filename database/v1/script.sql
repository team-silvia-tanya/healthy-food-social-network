create table emoticons
(
    emoticon_id int auto_increment
        primary key,
    emoji       varchar(100)         not null,
    enabled     tinyint(1) default 1 not null,
    constraint emoticons_emoji_uindex
        unique (emoji)
);

create table nationalities
(
    nationality_id int auto_increment
        primary key,
    nationality    varchar(50)          not null,
    enabled        tinyint(1) default 1 not null,
    constraint nationalities_nationality_uindex
        unique (nationality)
);

create table sex
(
    sex_id  int auto_increment
        primary key,
    type    varchar(50)          not null,
    enabled tinyint(1) default 1 not null,
    constraint sexs_type_uindex
        unique (type)
);

create table users
(
    username varchar(100)         not null,
    password varchar(68)          not null,
    enabled  tinyint(1) default 0 not null,
    constraint users_username_uindex
        unique (username)
);

alter table users
    add primary key (username);

create table authorities
(
    username  varchar(100) not null
        primary key,
    authority varchar(50)  not null,
    constraint authorities_pk
        unique (username, authority),
    constraint authorities_users_username_fk
        foreign key (username) references users (username)
);

create table confirmation_tokens
(
    confirmation_token_id bigint auto_increment
        primary key,
    confirmation_token    varchar(255) null,
    created_date          datetime     null,
    username              varchar(100) not null,
    constraint confirmation_tokens_users_username_fk
        foreign key (username) references users (username)
);

create table visibilities
(
    visibility_id int auto_increment
        primary key,
    type          varchar(50)          not null,
    enabled       tinyint(1) default 1 not null,
    constraint visabilities_type_uindex
        unique (type)
);

create table user_profile_pictures
(
    picture_id    bigint auto_increment
        primary key,
    picture       varchar(200)         not null,
    visibility_id int                  not null,
    enabled       tinyint(1) default 1 not null,
    constraint user_profile_pictures_visabilities_visability_id_fk
        foreign key (visibility_id) references visibilities (visibility_id)
);

create table users_details
(
    user_details_id bigint auto_increment
        primary key,
    email           varchar(100) not null,
    first_name      varchar(50)  not null,
    last_name       varchar(50)  not null,
    picture_id      bigint       not null,
    description     varchar(500) null,
    age             int          null,
    sex_id          int          null,
    nationality_id  int          null,
    constraint users_details_email_uindex
        unique (email),
    constraint users_details_nationalities_nationality_id_fk
        foreign key (nationality_id) references nationalities (nationality_id),
    constraint users_details_sexs_sex_id_fk
        foreign key (sex_id) references sex (sex_id),
    constraint users_details_user_profile_pictures_picture_id_fk
        foreign key (picture_id) references user_profile_pictures (picture_id),
    constraint users_details_users_username_fk
        foreign key (email) references users (username)
);

create table connections
(
    connection_id bigint auto_increment
        primary key,
    sender_id     bigint               not null,
    receiver_id   bigint               not null,
    enabled       tinyint(1) default 0 null,
    constraint connections_pk_2
        unique (sender_id, receiver_id),
    constraint connections_users_details_user_details_id_fk
        foreign key (sender_id) references users_details (user_details_id),
    constraint connections_users_details_user_details_id_fk_2
        foreign key (receiver_id) references users_details (user_details_id)
);

create table posts
(
    post_id       bigint auto_increment
        primary key,
    visibility_id int                          not null,
    creator_id    bigint                       not null,
    title         varchar(100)                 not null,
    description   varchar(5000)                null,
    picture       varchar(200)                 null,
    video         varchar(200)                 null,
    location      varchar(300)                 null,
    timestamp     datetime   default curtime() null,
    enabled       tinyint(1) default 1         null,
    constraint posts_users_details_user_details_id_fk
        foreign key (creator_id) references users_details (user_details_id),
    constraint posts_visabilities_visability_id_fk
        foreign key (visibility_id) references visibilities (visibility_id)
);

create table comments
(
    comment_id  bigint auto_increment
        primary key,
    post_id     bigint                       not null,
    creator_id  bigint                       not null,
    description varchar(500)                 not null,
    timestamp   datetime   default curtime() not null,
    enabled     tinyint(1) default 1         not null,
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint comments_users_details_user_details_id_fk
        foreign key (creator_id) references users_details (user_details_id)
);

create table comments_likes
(
    like_id         bigint auto_increment
        primary key,
    user_details_id bigint not null,
    comment_id      bigint not null,
    constraint likes_pk_2
        unique (user_details_id, comment_id),
    constraint comments_likes_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint likes_users_details_user_details_id_fk
        foreign key (user_details_id) references users_details (user_details_id)
);

create table posts_emoticons
(
    id          bigint auto_increment
        primary key,
    post_id     bigint not null,
    emoticon_id int    not null,
    constraint posts_emoticons_emoticons_emoticon_id_fk
        foreign key (emoticon_id) references emoticons (emoticon_id),
    constraint posts_emoticons_posts_post_id_fk
        foreign key (post_id) references posts (post_id)
);

create table posts_likes
(
    id              bigint auto_increment
        primary key,
    user_details_id bigint not null,
    post_id         bigint not null,
    constraint posts_likes_pk_2
        unique (user_details_id, post_id),
    constraint posts_likes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_likes_users_details_user_details_id_fk
        foreign key (user_details_id) references users_details (user_details_id)
);


