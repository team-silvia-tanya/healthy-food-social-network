INSERT INTO healthy_food.users (username, password, enabled) VALUES ('anor@abv.bg', '$2a$10$78SJz1xIhVnPTXav6VgxLOsaygbhulEsZKSK4g3l1w9jDpfWrnUoS', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('ca1234km@abv.bg', '$2a$10$eniW5yuk1WosAO46TQcVtOXjUl3ANGXvcRELCaDQ5OZOWkJms7v0O', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('g.kabadaev@gmail.com', '$2a$10$PrNLNOqGCI/uRzi8RVRs9exC9AhLR3VMdbvJsRkVqN35sHIaWrqVe', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('georgi.kabadaev@abv.bg', '$2a$10$j2pgTsvjmxnC49k0ZqcLNuXBpRRS7R6SENkE8E7z3yMeOfvvZXekq', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('healthy.food.social.network@gmail.com', '$2a$10$NqnW1tRMjZsqjxozEWVHd.I7u4FUw.jcQVihusOdwWHwsKP3czjpO', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('ilchavdarov@gmail.com', '$2a$10$CdTXCdik6ZGKjzbGSxxwOOKxviUWH11taDUvAtmUxp1qd6ydgfMl2', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('ivan.petrov.testov@gmail.com', '$2a$10$NK5y6UYemHQ3/YjU1IshMeQv2rc1sZ54m7UVXeCl2sjwm/75ijHfe', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('mariana_marinova.dr@abv.bg', '$2a$10$5zWBPmV9iZcClXIAHSCRxuSrC205LQUerBmhMNn5FpzFM8fcKWxfS', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('silviatdivanova@gmail.com', '$2a$10$DZm7MKD7ymVepQta9KDPZul.qey/6oAy8Mj8hV5wg4EyMqISZW3RC', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('tanya.serdon@gmail.com', '$2a$10$QdW4TlID.OAMA/w8mTLHm.0kZgCqIUCqlK99x0wKWN55Xr3Zn.U0C', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('tkabadaeva@bcserdon.com', '$2a$10$s7b11OK0ixIIhMn.6EWyrekk6Pa7YF7eWigu3hWaTadGdX.pdDoYe', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('tkabadaeva@fineline.bg', '$2a$10$vcajRpeQcKOQ0hSJIf5A8.vpJb0gmmQjOjBjDaKaXmsJ7aSOQY0uG', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('tkabadaeva@gmail.com', '$2a$10$uW42nZ3RLhoJrcnVtxaMle7lG85tdybVYKt3JyAYJ5TZ171n.3Le2', 1);
INSERT INTO healthy_food.users (username, password, enabled) VALUES ('ykabadaeva@gmail.com', '$2a$10$ij1mp3NXtFW4I4iSBuUrG.g7rZWm9csg.qxm1nPo3mtoGmcLGNZc2', 1);