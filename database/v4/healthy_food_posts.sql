INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (1, 1, 3, 'What is healthy living?', 'This article is designed to give tips to readers about how they can improve or augment actions in their life to have a healthy lifestyle; it is not meant to be all inclusive but will include major components that are considered to be parts of a lifestyle that lead to good health. In addition to the tips about what people should do for healthy living, the article will mention some of the tips about avoiding actions (the don''ts) that lead to unhealthy living.

"Healthy living" to most people means both physical and mental health are in balance or functioning well together in a person. In many instances, physical and mental health are closely linked, so that a change (good or bad) in one directly affects the other. Consequently, some of the tips will include suggestions for emotional and mental "healthy living."

Healthy eating (diet and nutrition)
All humans have to eat food for growth and maintenance of a healthy body, but we humans have different nutrition requirements as infants, children (kids), teenagers, young adults, adults, and seniors. For example, infants may require feeding every 4 hours until they gradually age and begin to take in more solid foods. Eventually they develop into the more normal pattern of eating three times per day as young kids. However, as most parents know, kids, teenagers, and young adults often snack between meals. Snacking is often not limited to these age groups because adults and seniors often do the same.
', 4, '2020-04-22 16:19:38', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (2, 1, 3, 'Instant Pot Cauliflower and Butternut Thai Curry', 'My very first vegan Instant Pot recipe is here! I finally took the plunge and purchased an Instant Pot after being on the fence about whether I wanted a new appliance to take up real estate on my counter (it would have to fight for space next to the kids’ piles of artwork, after all). Thanks again for the Ask Angela weigh-in back in February. I’m usually suspicious of new trends and like to wait a good while before I take the plunge, but I’m loooving it so far. I had totally underestimated how nice it is to put the lid on a recipe and walk away! But this same convenient feature also makes it challenging to develop recipes because you have ONE SHOT to get the cook time/pressure correct. No big deal. This curry took over 10 trials to get perfect…I changed up the flavours, cook time (6 minutes, 5 minutes, 1 minute, 4 minutes…ahh!), liquids-to-solids ratios…you name it, I tweaked it! Nicole and I love a challenge, though, so it’s been fun figuring it out and I do think we’ll get quicker as we go.', 5, '2020-04-22 14:55:10', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (3, 1, 3, 'New Year Glow Smoothie Bowl', 'Happy 2018, friends! If you’re still recovering from New Year celebrations (and/or a whole lotta holiday travel like us!), I have just the recipe to kick off your January. It’s refreshing and light with an immune-boosting bonus—something I think most of us could use right about now! I know I could.

We had a fun whirlwind of a holiday week! How did yours go? The four of us took a road trip to see Eric’s parents, then headed west to visit my family in Alberta. Since our stay at my parents’ house was only four nights, we opted to keep the kids on eastern time. I have to say this strategy worked out so much better than the last visit. Both kids slept well (aside from one night) and were pretty happy during the day!! If we were staying longer, Eric and I would’ve gradually shifted them to mountain time, but for this visit we didn’t see a need to. It felt like a huge win. Parents, how do you handle changing time zones with your kiddos in tow? I think this was the first small success we’ve had. (Still worth celebrating, though! Ha.)', 6, '2020-04-22 14:57:15', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (4, 1, 2, 'Crunchy Dill Chickpea Pancakes with Lemon-Garlic Aioli', 'Ingredients:
For the Lemon-Garlic Aioli:
1/2 cup (125 mL) soy-free vegan mayo
1 large or 2 medium garlic cloves, grated on microplane
1 to 2 teaspoons (5 to 10 mL) fresh lemon juice, to taste (I use 2)
For the pancakes:
1 tablespoon (15 mL) coconut oil or extra-virgin olive oil
3 large garlic cloves, minced (1 heaping tablespoon)
1/2 cup (42 g) grated peeled carrot (1/2 medium)*
1/3 cup (47 g) finely chopped dill pickle (2 small)**
1/2 cup (63 g) chickpea flour
2 tablespoons (10 g) nutritional yeast
1/2 cup (125 mL) water
Fine sea salt and pepper, to taste (I use 1/4 teaspoon)
For serving:
Sliced green onion, chopped dill pickle and fresh dill, aioli

Nutrition Information
Serving Size
1 of 7 pancakes w/ 1 tsp mayo each | Calories
90 calories | Total Fat
6 grams
Saturated Fat
2 grams | Sodium
160 milligrams | Total Carbohydrates
6 grams
Fiber
1 grams | Sugar
1 grams | Protein
2 grams', 7, '2020-04-22 15:56:05', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (5, 1, 2, 'Healthy Pineapple Smoothie', 'This healthy Pineapple Smoothie recipe is easy to make with only 5 ingredients in 5 minutes! One sip of this pineapple banana smoothie will make you feel like you’re on a tropical island, it’s like a beach vacation in a glass! Plus it’s dairy-free! When I am seriously jonesing for warm sandy beaches and calming ocean tides, I head into the kitchen and make this Pineapple Smoothie! It tastes like a healthy, kid-friendly pina colada and it’s like a beach vacation in a glass!

It’s easy to make with only 5 ingredients in 5 minutes! One sip of this pineapple banana smoothie will make you feel like you’re on a tropical island! Plus it’s dairy-free! ', 8, '2020-04-22 16:32:40', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (6, 1, 1, 'Super Green Pasta', 'If you need a speedy, weeknight supper with lots of veggies, this is the recipe for you! It takes just 25 minutes to make, gets three portions of greens in and tastes delicious. You can adapt the recipe too, it’s delicious with sautéed mushrooms or roasted broccoli stirred through.

INGREDIENTS
2 portions pasta, we use brown rice pasta
1 courgette, halved and sliced into half moon shapes
2 garlic cloves, peeled and roughly chopped
Juice of half a lemon
200g fresh spinach
125ml almond milk, or any other plant-based milk
150g peas, frozen or fresh
1 tablespoon olive oil
1 teaspoon nutritional yeast, optional
Pinch of sea salt

METHOD
Preheat oven to 180c, fan setting.
Place the courgette on a baking tray with a drizzle of olive oil and salt, cook for 10 minutes.
After 10 minutes, add the garlic and roast for another 5-10 minutes until golden, before removing from the oven and leaving to one side.
Put the pasta in a pan of boiling water and cook to the instructions on the pack. Once cooked, drain and leave to one side.
Place the almond milk, spinach and salt in a pan over a medium heat and cook until the spinach has wilted.
Once wilted, add the roasted garlic and blend using a hand blender (or pour the sauce into a normal blender) until smooth - if you’re adding nutritional yeast, add it to your blender now.
Mix through the courgettes and peas, and cook for another 5 minutes.
Finally, stir the lemon juice through through the pasta before serving.', 9, '2020-04-22 15:53:29', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (7, 1, 1, 'The Health And Fitness Benefits Of Sport', 'Looking to take up a new sport? It could be that be that these health and fitness benefits of playing sports might just be the extra push you need to get involved.

For sure, playing sports is a generally a fantastic way to improve your fitness and health. Many of us may not feel at home pounding away on a treadmill or working up a sweat in the gym, but we’ll happily chase a ball around endlessly while playing a game of some sort.

For most people, taking part in sport will improve your general health and wellbeing. There are plenty of reasons why you should become involved in sport with reduced body fat, bone strengthening, improved stamina and flexibility being some of the reasons why you should take up a sport.

The following are just some of the many health and fitness benefits of starting out in a new sport which we hope will apply to whatever sport you opt for:

Playing sports helps reduce body fat or controls your body weight.
Sports allow you will gain the satisfaction of developing your fitness and skills.
Sports can help you fight depression and anxiety.
Sports allows you to challenge yourself and set goals.
Playing sports helps strengthen bones.
Sports help aid coordination, balance and flexibility.
Many sports can help improves stamina and concentration.
Sports allow you to experience the highs and lows of both winning and losing!
Through sports you will meet people with a similar interest to yourself and are likely to gain many new friends.
Sports are a great way for families to get exercise together.
If you are sporty then you are more likely to have a healthy lifestyle.
If you are tempted to take up a new sport why not check out our sports section  and find a sport to suit you.', 10, '2020-04-22 16:31:53', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (8, 1, 3, '7 Recipes You Can Make In 5 Minutes', 'That looks great I''ll make it', 11, '2020-04-22 16:05:44', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (9, 1, 4, 'Chocolate Avocado Pudding', 'Ingredients
for 2 servings

2 large ripe avocados, cubed
3 tablespoons dark chocolate cocoa powder
½ cup dark chocolate (110 g), melted
¼ cup almond milk (60 mL)
2 tablespoons maple syrup
1 teaspoon vanilla extract
1 pinch salt, or sea salt
Preparation
Add the avocados to a blender or food processor, along with the cocoa powder, melted chocolate, almond milk, maple syrup, vanilla extract and salt.
Blend until smooth, scraping down sides as necessary.
Serve immediately, or chilled.
Enjoy!', 25, '2020-04-22 21:44:04', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (10, 1, 4, 'Blueberry Almond Instant Oatmeal', 'Ingredients
for 1 serving

½ cup instant oatmeal (50 g)
2 tablespoons freeze-dried blueberry
1 tablespoon slivered almond
1 tablespoon chia seed
? teaspoon vanilla bean
2 teaspoons sugar, of choice
¾ cup water (175 mL), or milk, boiling
Preparation
Add the instant oatmeal, freeze-dried blueberries, almonds, vanilla bean, chia seeds, and sugar into a mason jar.
Store in a cool, dry place for up to a month.
To prepare, pour boiling water or milk over oats. Stir to combine and let sit for 3 minutes.
Serve with slivered almonds and fresh blueberries.
Enjoy!', 26, '2020-04-22 21:46:00', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (11, 2, 4, 'VEGETARIAN STUFFED PEPPERS', 'Ingredients

4 Large Bell Peppers sliced in half and seeds removed
2 Tbsp Olive Oil
Kosher salt and pepper, to taste
1 Medium Onion diced
2 Garlic Cloves minced
¼ tsp Red Pepper Flakes optional
1 tsp Ground Cumin
1 Tsp Smoked Paprika
½ Tsp Cayenne Pepper optional
15 oz Diced Tomatoes one can
15 oz Black Beans drained and rinsed, one can
15 oz Red Beans drained and rinsed, one can
3 Cup Vegetable Broth
1 Cup Fresh/Frozen or Canned Corn
1 Cup Quinoa
½ Cup Shredded Monterey Jack Cheese
Optional toppings: Avocado, cilantro/parsley, sour cream, and lime wedges
Instructions
Preheat oven to 400° and line a baking tray with parchment paper. Thoroughly wash the bell pepper and pat the excess moisture with a paper towel.
Carefully slice each bell pepper halfway, through the stems. Discard the ribs and seeds, and place on the prepared baking tray.
Spray with a bit of cooking oil spray and sprinkle with salt and pepper. Bake for 8-10 minutes.
Meanwhile, heat oil in a heavy skillet, over medium-high heat. Add onion and garlic, sauté until onion is translucent.

Stir in tomatoes, beans, and corn. Add red pepper flakes, ground cumin, smoked paprika, salt and pepper, cook stirring constantly for 2-3 minutes. Stir in the quinoa and broth, and reduce the heat to low.
Cover and let it simmer until quinoa is cooked through about 12-15 minutes. Once done, remove from the heat and carefully spoon the mixture into the roasted peppers.
Generously sprinkle shredded cheese on top, then place back into the oven and cook for 6-8 minutes (until cheese is melted and peppers are tender).
Once done, remove from the oven, garnish with chopped cilantro or parsley. Serve with your favorite toppings and enjoy.
Nutrition
Calories: 341kcal | Carbohydrates: 53g | Protein: 16g | Fat: 8g | Saturated Fat: 2g | Cholesterol: 6mg | Sodium: 476mg | Potassium: 892mg | Fiber: 13g | Sugar: 6g | Vitamin A: 3065IU | Vitamin C: 113mg | Calcium: 121mg | Iron: 5mg', 27, '2020-04-22 21:48:58', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (12, 2, 8, 'HOW TO MAKE A DARK CHOCOLATE BERRY OATMEAL BAKE', 'INGREDIENTS
1 ripe banana, medium
2 eggs, large
1 teaspoon vanilla extract
1/4 cup maple syrup
3/4 cup unsweetened almond milk
DRY
2 cups rolled oats
1/2 cup white whole wheat flour (any kind of flour will work)
1 teaspoon baking powder
1 pint Driscoll’s raspberries
1 pint Driscoll’s blackberries
1/3 cup mini dark chocolate chips
pinch of salt
INSTRUCTIONS
Preheat oven to 350ºF and spray a 10-inch by 7-inch casserole dish with nonstick cooking spray.
Place banana in a large bowl and mash with a fork. Then, add the rest of the wet ingredients and mix until combined.
Add rolled oats, white whole wheat flour, baking powder, berries, and a pinch of salt and mix. Then, add mini chocolate chips and mix again.

Transfer batter into casserole dish and use a spatula to spread evenly. Add additional berries to the top, if desired.
Bake at 350ºF for 30 minutes. Then, add more chocolate chips if desired and bake for an additional 5 minutes.
NOTES
The topping on this oatmeal bake is all-natural peanut butter.
NUTRITION
Serving Size: 1/6 recipeCalories: 342Sugar: 19Fat: 9Carbohydrates: 52Fiber: 12Protein: 9', 28, '2020-04-22 21:55:59', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (13, 1, 8, 'Eating Well Without Being Miserable', 'Eating a healthy diet is another part of a healthy lifestyle. Not only can clean diet help with weight management, but it can also improve your health and quality of life as you get older.4? You already know about the food groups and the fact that you should eat more fruits and vegetables and less processed foods. You probably have a list of things you know you should do for a healthier diet but, again, making too many changes at once can backfire. Going on a restrictive diet may make you crave the very foods you''re trying to avoid.?

Another approach is to look for ways to make smaller changes each day.4? Just a few ideas for changing how you eat include:

Eat more fruit. Add it to your cereal, salads, dinners, or make it a dessert. The fruit is also a great snack after work or school to keep you going for dinner.
Sneak in more veggies. Add them wherever you can—a tomato on your sandwich, peppers on your pizza, or extra veggies in your pasta sauce. Keep pre-cut, canned, or frozen veggies ready for quick snacks.
Try a healthy salad dressing. If you eat full-fat dressing, switch to something lighter and you''ll automatically eat fewer calories.
Eat low-fat or fat-free dairy. Switching to skim milk or fat-free yogurt is another simple way to eat fewer calories without having to change too much in your diet.
Make some substitutions. Look through your cabinets or fridge and pick three foods you eat every day. Write down the nutritional content and, the next time you''re at the grocery store, find lower-calorie substitutes for just those three items.', 29, '2020-04-22 22:00:11', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (14, 1, 8, 'All About Avocados', 'Avocados are the darling of the produce section. They’re the go-to ingredient for guacamole dips at parties. And they''re also turning up in everything from salads and wraps to smoothies and even brownies. So what, exactly, makes this pear-shaped berry (yes, that’s right!) such a super food?

Nutrient All-Star
Avocados offer nearly 20 vitamins and minerals in every serving, including potassium (which helps control blood pressure), lutein (which is good for your eyes), and folate (which is crucial for cell repair and during pregnancy).

Avocados are a good source of B vitamins, which help you fight off disease and infection. They also give you vitamins C and E, plus natural plant chemicals that may help prevent cancer.

Avocados are low in sugar. And they contain fiber, which helps you feel full longer. In one study, people who added a fresh avocado half to their lunch were less interested in eating during the next three hours.

The Skinny on the Fat and Calories
Avocados are high in fat. But it''s monounsaturated fat, which is a "good" fat that helps lower bad cholesterol, as long as you eat them in moderation.

CONTINUE READING BELOW
YOU MIGHT LIKE
Avocados have a lot of calories. The recommended serving size is smaller than you’d expect: 1/3 of a medium avocado (50 grams 0r 1.7 ounces). One ounce has 50 calories.

How to Prepare Avocados
Store avocados at room temperature, keeping in mind that they can take 4 to 5 days to ripen. To speed up the ripening process, put them in a paper bag along with an apple or banana. When the outside skins are black or dark purple and yield to gentle pressure, they’re ready to eat or refrigerate.

Wash them before cutting so dirt and bacteria aren’t transferred from the knife onto the pulp.

While guacamole is arguably the most popular way to eat avocado, you can also puree and toss with pasta, substitute for butter or oil in your favorite baked good recipes, or spread or slice onto sandwiches.

When ordering at a restaurant, remember that not all avocado dishes are created equal. Some items -- like avocado fries and avocado egg rolls -- are coated in batter and fried, making them much higher in both calories and fat.

Allergic to Latex?
If you have a latex allergy, talk to your doctor before adding avocado to your diet. People with a serious allergy to latex may also experience symptoms after eating avocado.', 30, '2020-04-22 22:05:22', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (15, 2, 9, 'Avocado panzanella', 'Ingredients
800g mix of ripe tomatoes
1 garlic clove, crushed
1½ tbsp capers, drained and rinsed
1 ripe avocado, stoned, peeled and chopped
1 small red onion, very thinly sliced
175g ciabatta or crusty loaf
4 tbsp extra virgin olive oil
2 tbsp red wine vinegar
small handful basil leaves
Method
Halve or roughly chop the tomatoes (depending on size) and put them in a bowl. Season well and add the garlic, capers, avocado and onion, and mix well. Set aside for 10 mins.

Meanwhile, tear or slice the ciabatta into 3cm chunks and place in a large serving bowl or on a platter. Drizzle with half the olive oil, half the vinegar and add some seasoning. When ready to serve, pour over the tomatoes and any juices. Scatter with the basil leaves and drizzle over the remaining oil and vinegar. Give it a final stir and serve immediately.', 31, '2020-04-22 22:10:33', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (16, 1, 9, 'ZABAGLIONE TRIFFLES', 'Serves 2

Ingredients:

4 egg yolks, room temperature

1/4 cup dry marsala

1/4 cup sugar

2 biscotti, cubed

1 cup strawberries, sliced

Directions:

Fill a large saucepan with 1-inch of water and bring to a simmer. Put the egg yolks, marsala, and sugar into a large stainless-steel bowl and place the bowl over the saucepan. Using a whisk, beat the egg-yolk mixture until it has thickened but still forms a ribbon when the beaters are lifted, about 5-8 minutes.
Place the biscotti into the bottom of 2 stemmed glasses or bowls. Top with strawberries and immediately pour the hot zabaglione over them. Serve immediately or refrigerate it for up to an hour.', 32, '2020-04-22 22:13:44', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (17, 1, 9, 'The BEST Easy Healthy Lasagna Recipe', 'Ingredients
1/2 lb ground beef
1/4 onion (chopped)
2 zucchini (medium)
1 summer squash (medium)
15 oz. whole tomatoes (canned)
10 oz. tomato sauce
1/2 tsp garlic powder
1/2 tsp dried parsley
1 tsp dried basil
6 uncooked lasagna noodles
10 oz. cottage cheese (small curd)
1/2 cup parmesan (grated)
1 cup mozzarella (shredded)
salt/pepper

Instructions
Preheat oven to 375 degrees.
Cook the chopped onion and ground beef until the beef is fully cooked.
While the beef is cooking, cut the zucchini and summer squash into small, 1/4" - 1/2" cubes.
Once the beef is done cooking, set aside and drain.
Cook the zucchini and summer squash for about 10 minutes.
Add in the tomatoes, tomato sauce, and spices.
Continue cooking over medium heat for another 15 minutes, stirring often.
Add the cooked beef and onions back into the tomato sauce mixture and stir.
Pour about 1/2 of the tomato sauce mixture into a 8" x 10" casserole dish.
Place 3 of the lasagna noodles on top of the mixture (breaking each one in half if necessary), pressing each one down so that it''s covered with about 1/4" of the tomato mixture on top.
Add 1/2 of the cottage cheese to the casserole dish.
Top with 1/2 of the parmesan and 1/2 of the mozzarella.
Repeat steps 9-12 with the remaining ingredients.
Cover the casserole dish with foil and bake for 30 minutes.
Uncover and bake for another 15 minutes.
Serve warm and enjoy!
Notes
Carbs: 35  Fat: 10  Protein: 26', 33, '2020-04-22 22:15:34', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (18, 2, 5, 'HEALTHY GLUTEN FREE LEMON BARS', 'INGREDIENTS
FOR THE CRUST
1.5 cups super fine almond flour
2 tablespoons melted coconut oil
2 tablespoons honey
1 teaspoon vanilla extract
1/8 teaspoon salt
FOR THE FILLING
4 large eggs
1/2 cup lemon juice
1/2 cup honey
4 teaspoons coconut flour
INSTRUCTIONS
Preheat oven to 350ºF and line an 8-inch x 8-inch cake pan with parchment paper. Set aside.
Prepare the crust by mixing together all ingredients, forming a ball. Transfer dough into pan and use your hands and/or a spatula to spread to the edges of the pan. The key is to make sure the crust is relatively the same thickness all around, so the spatula comes in handy especially for the edges.
Bake crust for 10 minutes at 350ºF.
While the crust is baking, prepare filling. Whisk 4 eggs in a medium-size bowl and then add the rest of the ingredients. Whisk again until there are no more coconut flour chunks.
Once crust has baked for 10 minutes, remove from oven and pour on filling. Place back in oven and bake for an additional 18-20 minutes.*
Remove from oven and let sit on the stove top for 10 minutes before transferring into the fridge. Chill for 2 hours before slicing.
Slice into 16 squares and serve with a sprinkle of organic powdered sugar.
NOTES
The bake time for the filling depends on your oven. Keep a close eye on it! If it cracks in the center, don’t worry about it :)', 34, '2020-04-22 22:19:30', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (19, 1, 5, 'Ragi pancakes recipe for babies, toddlers, and kids', 'Ingredients
Ragi flour - 1/2 cup
Organic Jaggery powder - 1 to 2 tbsp optional
Coconut scrapped - 2 tbsp
Banana - 1 medium-sized mashed
Cardamom - 1
Cashews - 1 tbsp optional
Water - as needed
Salt - a pinch
Oil/ghee - for making dosas
Instructions
Preparation
Sterilize the bowls and spoons used for feeding your baby in a vessel with hot water for five minutes and keep it immersed in the same vessel until use.
Method
Measure ragi flour, coconut & Jaggery. Keep it ready. Mash the bananas with a fork and set aside.
Take jaggery in a saucepan & add water till it immersed level. Heat the jaggery until it melts completely. Strain the jaggery water using a strainer to remove impurities. Set aside.
In a vessel, add ragi flour, jaggery water, scrapped coconut, crushed cardamom, and mashed banana. Add water little by little and mix well so that no lumps are formed. The batter should be like regular dosa batter consistency neither too thick nor runny. Finally, add cashews to the mixture and mix well. Set aside.
Heat a dosa tawa, add a ladle of batter to it, and spread it to form a circle. Don''t spread too much. Sprinkle ghee or oil. Cook until brown on one side for five minutes and flip the dosa. Cook for a minute and remove from tawa. Repeat the same process for the remaining batter.', 35, '2020-04-22 22:24:38', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (20, 1, 6, 'Joga at home', '', 36, '2020-04-22 22:33:21', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (21, 2, 6, 'HOMEMADE FAJITA SEASONING', 'When I am craving fajitas I always forget to pick up a fajita packet from the grocery store. Like, there is nothing worse when you’re about to make some chicken fajitas and and you have no chicken fajita seasoning at home to use.

Have no fear, making your own fajita spice mix in your own kitchen is so simple! You need 6 basic spices, plus a cute little jar to house your spice mix.

Step 1: Collect Fajita Spices
Like I mentioned above, you only need 6 different spices to make a basic homemade fajita seasoning. Chances are, you already have these in your cabinet, too!

chili powder
ground cumin
garlic powder
paprika
salt
pepper
Step 2: Mix Seasoning
Mix your fajita seasoning together in a small bowl. This is the step when you can double or triple the recipe so that you can have more for later!

Spice Tip: I wanted to mention that this specific recipe isn’t super spicy. If you like a lot of spice, you can up your paprika by 1/2 to 1 teaspoon or add a pinch or two of cayenne pepper!


Step 3: Transfer into Jar & Seal
I like to save old spice jars just in case I want to make my own mix like this one! If you don’t have any spice jars laying around, you can always store your fajita seasoning in a plastic baggie!', 37, '2020-04-22 22:36:26', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (22, 1, 10, 'Fit ball workout', '', 38, '2020-04-22 22:39:11', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (23, 1, 10, 'HOMEMADE GUACAMOLE', 'Recipe
HOMEMADE GUACAMOLE RECIPE

PRINT RECIPE
LEAVE A REVIEW
?????

5 FROM 1 REVIEWS

This is hands down the BEST guacamole recipe on the internet and it only requires 6 ingredients and 5 minutes!

PREP TIME:
10 minutes
COOK TIME:
0 minutes
TOTAL TIME:
10 minutes
AUTHOR: Lee Hersh
YIELD: 4-6 1x
CATEGORY: Appetizer
METHOD: No Bake
CUISINE: Mexican

INGREDIENTS
3 large avocados, ripe
3 cloves garlic, smashed and minced (~1.5 tablespoons)
1/3 medium red onion, finely diced
1 medium lime, juice (or more, to taste)
1/4 teaspoon sea salt (or more, to taste)
2 tablespoons cilantro, freshly chopped
optional: 1/2 large jalapeno, finely diced
INSTRUCTIONS
First, remove the flesh of the avocados and place it into a medium-size bowl. Use a fork to mash avocado flesh until smooth. Alternatively, you can leave some chunks, if you prefer a more chunky guacamole.
Next, add in minced garlic, finely diced onion, lime juice, sea salt, and cilantro, and mix until combined.
Give guacamole a taste test and adjust the salt and lime juice depending on what it needs.
Serve with your favorite tortilla chips!
NOTES
Storage: We recommend eating guacamole immediately after you make it. However, if you have leftovers, place guacamole into a medium bowl and squeeze on some lime juice. Then, cover it with a piece of plastic wrap and remove as much air as possible. Store in the fridge for 2-3 days.
Nutrition information does not include chips.
NUTRITION
Serving Size: 1/4Calories: 182Sugar: 1Fat: 16Carbohydrates: 11Fiber: 8Protein: 5', 39, '2020-04-22 22:41:37', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (24, 2, 12, 'BEST PROTEIN SHAKES', 'NUTS/NUT BUTTER
Nuts and nut butters are a favorite and delicious protein source to add to your protein shake. Not only will you get protein, but you’ll also pack in the fiber and healthy fats, too!
SEEDS
Whether you’re topping your protein shake or adding them right into the mix, seeds are an excellent vegan, paleo, and keto-friendly protein addition!
FRUIT/VEGGIES
Most protein shakes begin with the base of frozen fruit because it’s easy to blend up and super flavorful. Here, I wanted to point out some of the highest-protein fruits and veggies out there that you can also add to your protein smoothie to really amp it up.
High-Protein Fruit


Guava (1 cup) – 4g protein
Avocado (1 medium avocado) – 4g protein
Kiwi (1 cup) – 2g protein
Blackberries (1 cup) – 2g protein
High-Protein Veggies

Spinach (1 cup) – 5g protein
Kale (1 cup) – 3g protein
Sweet Potato (1/2 cup, mashed) – 2.5g protein
Cauliflower (1 cup) – 2g protein
OTHER PROTEIN SOURCES
We didn’t want to leave out some other easy and tasty protein sources that you can add to your shakes to get an extra boost of protein! Greek yogurt is a personal favorite of mine and makes your smoothies super creamy!', 40, '2020-04-22 22:47:23', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (25, 2, 12, 'TRIPLE BERRY PROTEIN SMOOTHIE', 'INGREDIENTS
1.5 cups frozen triple berry mix
1 medium frozen banana
1/2 tablespoon chia seeds
1/4 cup vanilla protein powder
1.25 cups unsweetened almond milk
INSTRUCTIONS
Place all ingredients in a high-speed blender and mix until smooth.
NOTES
Option to add more almond milk by the ¼ cup depending on how thick or thin you like your smoothies.
NUTRITION
Serving Size: 1/2 recipeCalories: 196Sugar: 16Fat: 3Carbohydrates: 32Fiber: 8Protein: 12', 41, '2020-04-22 22:48:57', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (26, 1, 7, 'Healthy & Affordable School Lunch Ideas', 'Sometimes, all you need is a little inspiration when it comes to lunch prep. Most of the ideas below are customizable, and it’s easy to mix and match ingredients and sides based on what your children love to eat (and what they refuse to touch).

Keep in mind that it’s often worthwhile to purchase organic fruits and vegetables when you can. While organic items often cost more, there are plenty of ways to buy organic on a budget.

1. Quesadillas With Fresh Fruit
Most kids love quesadillas, and they’re a great option for a healthy and affordable lunch. They’re also quick to make in the morning and taste just as good cold as they do hot.

Ingredients:

One flour tortilla
Shredded chicken (I make the most of leftovers by using rotisserie chicken from the night before)
Shredded cheese
Avocado
To Make:

Place tortilla on a non-stick pan over medium heat.
Add shredded cheese, chicken, and avocado to one side of the tortilla.
Let most of the cheese melt, then fold over the tortilla, pressing the two halves together. Flip the tortilla and let the other side get a bit crispy before you take it off the heat.
Pack With:

Fresh fruit
Grape tomatoes with ranch dressing
Tip: You can also add some refried beans or black beans to the quesadilla to increase fiber and protein content.

2. Peanut Butter & Bacon Wrap
Yes, you read that right. I’m about to combine peanut butter, bacon, and bananas into a holy trinity that your kids will be happy to eat several days in a row.

Ingredients:

One whole-wheat wrap
Peanut butter
Chopped, cooked bacon
Sliced bananas
Honey
To Make:

Smear the whole-wheat wrap with peanut butter, then sprinkle it lightly with bacon pieces.
Drizzle a very thin layer of honey and then add several slices of banana.
Roll it up and slice.
Pack With:

Red grapes
Carrot sticks and dipping sauce, such as ranch dressing
Tip: You don’t need a lot of bacon or honey for this wrap. Both have strong flavors and plenty of calories, so you only need a bit to make an impact.

3. Not-Quite-a-Sandwich Roll-Ups
Sure, this is almost a sandwich, but your kids won’t realize it because it has no bread. It’s also easy to sneak a few greens into the middle.

Ingredients:

Turkey or roast beef lunch meat
Sliced Swiss cheese or a cheese stick
Spinach
Mayo or avocado (optional)
To Make:

If you choose to include mayo or avocado, spread it directly on the turkey or roast beef.
Add a thin layer of spinach, then put the sliced cheese or cheese stick on top.
Roll everything up, and you’re good to go.
Pack With:

Apple slices
Whole-grain crackers
Salted edamame
Tip: To switch things up, make sandwich kabobs instead of roll-ups. If you have younger children, use bamboo sticks, which have rounded ends, rather than toothpicks. For kabobs, it will be easier to use cubed cheese rather than sliced. Add grape tomatoes, romaine lettuce, and baby dill pickles to liven things up.

4. Hummus & Veggies
Kids love to dip things, which is why the marriage of hummus and veggies is perfect for school lunches.

Ingredients:

Grape tomatoes, celery sticks, baby carrots, sliced cucumbers
A large dollop of hummus
To Make:

Assemble ingredients in separate containers in your kid’s lunchbox (this is when a bento box is ideal).
Pack With:

A whole-wheat pita or crackers
A protein, such as leftover rotisserie chicken, cubed turkey, or a hard-boiled egg (if you have any on hand, throw in a tiny salt-and-pepper packet left over from last week’s takeout to use on the egg)
Tiny pickles
Pineapple or watermelon chunks
Blue corn chips and extra hummus for dipping
Tip: This lunch is perfect for mornings when you’re running late. I keep pre-sliced veggies in the refrigerator (buy them and slice them the weekend before) for mornings when we just don’t have it together. You can also turn this into a sandwich with a whole-grain pita pocket; simply smear the inside with hummus and add spinach, cucumbers, and other veggies your kids like.', 42, '2020-04-22 22:53:28', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (27, 2, 7, 'Some Ideas To Help Your Kids Eat More Fruits Without Getting Bored:', 'Here are some simple yet very useful tips to help parents include more fruits in their kids’ diets:

Fresh fruit is better than fruit juice. While the whole fruit has some of the natural sugars which will make it taste sweet. It also contains a lot of vitamins, fiber and minerals which will make it more nutritious than a cup of fruit juice. A small cup of juice will provide a child daily amount of vitamin C. However, some kids regularly consumes large amount of fruit juice and this can make them put on excess weight.
Choose fruits that are in season as they will be cheaper and more importantly, taste better.
Always have a bowl of fresh fruits in your home so that your kids can eat whenever they are hungry or want something to snack on.
Be creative in the way you prepare and serve fruits such as raw, sliced, mashed, baked, or serve some different colored fruits in different serving bowls and plates.
Add fruits to your kids’ breakfast cereal.
Snack on fruits: stewed fruit, frozen fruit, fruit crumble, or cakes and muffins made with fruits.
Make a fruit smoothie with frozen, fresh or canned (unsweetened juice) fruits, and then mix it with reduced- fat milk and yogurts.
Add chopped fruits to plain yogurts.
In the summer, freeze fruits or mix with yogurt before freezing for a snack.
Make fruit-based desserts such as crumble or baked fruit, stewed or poached fruit and serve with a little reduced-fat custard.
To get the greatest benefit, make sure that you give them many different juices each week instead of only one type.', 43, '2020-04-22 22:56:21', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (28, 1, 11, '9 Amazing Health Benefits of Berries', '1 Berries Keep You Mentally Sharp
Women who eat about two servings of strawberries or one serving of blueberries a week experienced less mental decline over time than peers who went without these nutrition powerhouses, research published in the Annals of Neurology found. In the study, researchers reviewed data from 16,010 women over age 70. Those with the highest berry intake postponed cognitive decline by about two and a half years. “We think that the effect might be related to a class of compounds called anthocyanidins, which is a type of flavonoid," explains study author Elizabeth Devore, ScD, an associate epidemiologist at Brigham and Women''s Hospital and a professor at Harvard Medical School in Boston. "These compounds, found almost exclusively in berries, are known to cross the blood-brain barrier and locate in learning and memory centers in the brain."

2 Berries Help Manage Diabetes
Berries are sweet, but not the kind of sweet that should send people with diabetes running. “Because they come with fiber, they can use that in a diabetic diet as a serving of fruit,” Copperman says. In general, it’s better to eat fruit whole rather than drink juice, which is much higher in sugar and doesn’t contain fiber. And even though the health benefits of berries still count when they are included in another food, the nutrition boost is better when you choose fresh blueberries over a blueberry pie or a muffin.

3 Berries Might Prevent Parkinson''s
People who eat at least two servings of berries a week have a 25 percent less chance of developing Parkinson’s disease than their peers, according to research published in the journal Neurology. The same research showed that men with the highest intake of flavonoids — which are abundant in berries — reduced their risk by 40 percent. Besides eating them plain, you can also get your berry servings by adding them fresh or frozen to other high-nutrition foods such as yogurt, oatmeal, and salads.

4 Berries Could Boost Heart Health
Even people with a strong inherited risk for heart disease may find that a diet rich in raw fruits and vegetables, including berries, can reduce their chances of having a heart attack, according to a study published in PLoS Medicine. Researchers looked at diet and heart attack numbers among 27,000 people of diverse backgrounds and found that loading up on fruits and vegetables seems to modify the genetic risk carried in the 9p21 gene, known to be related to heart disease. The research supports Copperman’s primary nutrition advice to patients: “Eat a rainbow.” Beyond the blueberries, include as many different-colored fruits and veggies in your diet as possible.
5 Berries for Weight Control
Because of their fiber and liquid content, berries give us a sense of fullness, Copperman says, and feeling sated is an important part of managing your diet program. Let kitchen creativity lead you to experimenting with berries in nutrition-rich recipes such as fresh fruit sauces and salad dressings, pair them with almonds for a fast snack, or eat them plain. “Emulsify them and make them part of a fruit vinaigrette rather than using a lot of oil,” Copperman suggests.

6 Berries for Lower Blood Pressure
Although no clinical trial has yet provided a berry prescription to fight high blood pressure, dietitians and physicians alike recommend a diet high in fresh fruits and vegetables to keep numbers in line. “This benefit goes back to the antioxidant properties they all share, and also your genetic predisposition,” Copperman says, pointing out that a berry-rich diet may be particularly helpful for people whose family history is loaded with heart disease risk (high blood pressure itself is a heart disease risk factor). The chemical compounds in berries fight the systemic inflammation that may accompany high blood pressure, helping to make your body healthier overall.

7 Fight Cancer With Berries
Flavonoid-packed berries, such as blueberries and raspberries, may one day lead to more effective cancer-prevention strategies. Research published in the Journal of Pharmaceutical and Biomedical Analysis suggests that flavonoids and other compounds found in berries may help reduce colon cancer risk. Because cancer prevention diets generally emphasize fruits and vegetables, including berries certainly won’t hurt. Again, Copperman stresses, any cancer-preventing benefit is enhanced by the inclusion of many different colors of fruits and veggies.

8 Eat Berries for Alzheimer''s Prevention
Berries’ antioxidants might be allies in the fight against the effects of age on the brain that might be behind changes leading to the cognitive decline associated with Alzheimer’s. Data reported at a recent meeting of the American Chemical Society suggests that berries contain chemical compounds called polyphenolics, which could help prevent Alzheimer’s disease by cleaning up the damaging build-up of toxins over time.

9 Fight Urinary Tract Infections With Berries
Cranberries are the berry most closely associated with urinary tract health, but Copperman says that nutrition-dense blueberries seem to be helpful as well. Because it’s a good idea to eat a variety of foods instead of overdoing it on just one kind, include both cranberries and blueberries (and many other berries) in your overall diet plan. And though it''s generally recommended to eat whole berries, at least in this instance, drinking juice might be helpful as well.', 44, '2020-04-22 23:04:40', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (29, 1, 11, 'Vegetables and Fruits', 'Vegetables and fruits are an important part of a healthy diet, and variety is as important as quantity.
No single fruit or vegetable provides all of the nutrients you need to be healthy. Eat plenty every day.
A diet rich in vegetables and fruits can lower blood pressure, reduce the risk of heart disease and stroke, prevent some types of cancer, lower risk of eye and digestive problems, and have a positive effect upon blood sugar, which can help keep appetite in check. Eating non-starchy vegetables and fruits like apples, pears, and green leafy vegetables may even promote weight loss. [1] Their low glycemic loads prevent blood sugar spikes that can increase hunger.

At least nine different families of fruits and vegetables exist, each with potentially hundreds of different plant compounds that are beneficial to health. Eat a variety of types and colors of produce in order to give your body the mix of nutrients it needs. This not only ensures a greater diversity of beneficial plant chemicals but also creates eye-appealing meals.', 45, '2020-04-22 23:08:30', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (30, 1, 13, 'AVOCADO BAKED EGGS', 'INGREDIENTS
3 large ripe avocados (room temperature)
6 medium eggs*
optional toppings
INSTRUCTIONS
First, preheat oven to 425ºF and spray 2 loaf pans* with nonstick cooking spray. 3 avocado halves will fit perfectly into 1 loaf pan.
Next, slice 3 large avocados in half (hotdog style) and remove the pits.
Then, remove 1 tablespoon to 1.5 tablespoons of flesh from each half, making sure to go wide, not deep. The reason for this is so that more egg can fit in each avocado half. Make sure not to waste the flesh. We recommend eating it :D
Place avocado halves inside loaf pan, lining them up next to each other so they don’t wiggle. Then, crack an egg into each half. Some egg white might spill over the top, that is okay! We found that medium-sized eggs worked best and large/jumbo were too big.
Season the tops of your eggs with salt and pepper.
Then, bake at 425ºF for 18-22 minutes or until your eggs reach the desired consistency.
Top with your favorite toppings and enjoy!
NOTES
eggs: large eggs will work too, but you might have some overflow egg white.
loaf pan: loaf pans seemed to work perfectly in order to keep the avocado halves stable so the egg doesn’t tip out. If you only have 1 loaf pan, just do 2 rounds of baking.
NUTRITION

Serving Size: 1/6Calories: 320Sugar: 0Sodium: 75Fat: 28Carbohydrates: 14Fiber: 10Protein: 8', 46, '2020-04-22 23:18:17', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (31, 1, 13, '9 Shocking Fitness Facts', '1
Your Gym Membership Won’t Counteract The Hours Spent Sitting
Think your fitness class or gym session makes up for the fact you sit at a desk all day? Think again. According to a number of research studies, sitting for long periods of time puts you at risk of dangerous illnesses such as heart disease and diabetes, even if you are otherwise physically active. While you may spend an hour at the gym or playing sports each day, think about what you are doing for the other 23 hours. It is important to make an effort to move more often throughout the day in any way you can; even the smallest activities done regularly can make a difference to your health.

2
Exercise Won’t Make You Thin
Many people use exercise as their sole method of weight management, however studies have shown that exercise, when not combined with dietary changes, does very little in respect to losing weight. A study published in The British Journal of Sports Medicine found that when a group of obese people completed 12 weeks of supervised cardio workouts without dieting, most did not experience any significant weight loss results. However, it is important to remember that, while it may not lead to weight loss by itself, exercise still has plenty of other health benefits.

3
Getting Active Helps You To Achieve Your Goals
Getting started in exercise won’t just improve your body and health, it can also help you to achieve other life goals. A study of people training for a marathon in New York found that regular exercise improves people’s goal-setting, organization and discipline - both at work and in other areas of life. Kicking your laziness habit could also help you to ditch other vices; a study in the Archives of Internal Medicine showed that smokers who exercised were twice as likely to quit and stay cigarette free than those who didn’t do any exercise.

4
Stretching Before Running May Lower Your Endurance
Many of us have been taught to warm up before exercise, however recent studies suggest that stretching before a run may not be beneficial to your workout. A study published in the Journal of Strength and Conditioning Research found that stretching before going for a run made a runner’s body less efficient so that they did not perform as well and were unable to run as far. Instead of stretching, try warming up with a walk and running-specific moves to mobilise your joints before your run. You should then make sure to include stretches as part of your post-run routine.

5
Facebook Can Boost Your Fitness Motivation
You may not have thought that browsing Facebook could help with your exercise goals, yet research suggests that signing up to the site could seriously boost your fitness motivation. Firstly, the constant lurking danger of being tagged in an unflattering photo means that social media sites have become a huge weight loss trigger for many of us. In fact, a study by Fitbit identified unflattering Facebook photos as the new number one weight loss trigger for Brits. Furthermore, more research from Fitbit has revealed that 64 per cent of those surveyed felt motivated to workout as a result of fitness boasts made by their friends on Facebook.

6
Morning Exercise Could Make You Ill
While daily exercise is great for your health, research suggests that night owls may have an advantage over early-bird exercisers. A study by a researcher from Brunel University, Middlesex, found that heavy training sessions early in the morning compromised the immune system and put athletes at increased risk of infection from bacteria and viruses. While a morning jog or gentle exercise session is unlikely to put you at great risk - and may in fact be a great way to start the day - it may be best to save heavier workouts for later in the day.

7
Women And Men Respond To Exercise In Different Ways
Exercise is a great activity to engage in as a couple, however it may not be a good idea for women to follow the exact same workout regime as their partner. A new study by researchers at the University of Missouri shows that women and men respond to exercise and diet in different ways, and that women need to do a lot more exercise and pay more attention to their diet to reap the same results as men. While exercising with your partner is a great way to stay motivated, try to tailor your workout to get the results you want.

8
Married Couples Do Significantly Less Exercise
While marriage comes with plenty of health benefits, research suggests that singletons are much more likely than married people to follow a regular exercise routine. The poll commissioned by the Department of Health found that only 27 per cent of adults questioned met the recommended 150 minutes of physical activity per week and of these 76 per cent of the men and 63 per cent of the women were married. The researchers suggested that this is because married people “let themselves go” as they become more comfortable with each other.

9
60 Per Cent Of Gym Memberships Go Unused
While joining a gym is one of the most common New Year’s resolutions, research suggests that, as gym attendance tends to go back to normal by mid-February, 60 per cent of gym memberships go unused. Rather than wasting money on unused memberships, new exercisers can make the most of opportunities to exercise for free or on the cheap by heading out for a run or hike, trying free taster sessions of classes they might enjoy, or signing up for pay-per-class fitness classes.', 47, '2020-04-22 23:17:30', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (32, 2, 14, '41 Healthy Foods Under $1', 'Protein
1. Pinto Beans
Price: $0.30 per 1/2 cup, $3 per can If you’re a big fan of ordering refried beans at restaurants, you’ll be glad to hear that they’re a snap to make at home: Just mash up pinto beans with garlic and spices on the stove. Packed with protein and fiber, pinto beans are a delicious and health-minded addition to any homemade burrito, soup, or salad.

2. Eggs
Price: $0.19 per egg, $3.50 per dozen When in need of some protein, eggs are a quick fix.The changing face of functional foods. Hasler CM. Journal of the American College of Nutrition, 2001, Jun.;19(5 Suppl):0731-5724.” data-widget=”linkref Scramble with veggies, add to a crepe, or make a frittata.

3. Almonds
Price: $0.60 per ounce (20-25 nuts), $5 per 8-ounce bag Grab a small handful of almonds during the day or add to a bowl of cereal or oatmeal for an extra-filling kick of protein. Rich in monounsaturated fat and fiber too, these super nuts could reduce the risk of diabetes and aid in weight loss.Health benefits of almonds beyond cholesterol reduction. Kamil A, Chen CY. Journal of agricultural and food chemistry, 2012, Feb.;60(27):1520-5118.” data-widget=”linkref

4. Peanuts
Price: $0.50 per ounce (25-30 nuts), $4 per 8-ounce bag Though some peanut butters are packed with sugar, in their natural form, these legumes can be healthy treat. When eaten in moderation, peanuts supply a dose of healthy fats and can reduce the risk of heart disease.Impact of peanuts and tree nuts on body weight and healthy weight loss in adults. Mattes RD, Kris-Etherton PM, Foster GD. The Journal of nutrition, 2008, Sep.;138(9):1541-6100.” data-widget=”linkref

5. Chicken Breasts
Price: $0.75 for 4-ounces, $2.99 per pound Forgo fast-food nuggets: A small, fresh chicken breast is cheaper and filled with healthy, lean protein. Make your own gyro with tzatziki or try any of these other nonboring ideas.

6. Black Beans
Price: $0.30 per 1/2 cup, $1.50 per can These unassuming beans pack a ton of fiber, as well as calcium, potassium, and folic acid. Pro tip: Buy dry beans for an even better health deal. Boiling them at home may preserve more of their cancer-fighting antioxidants.Total phenolic content and antioxidant properties of eclipse black beans (Phaseolus vulgaris L.) as affected by processing methods. Xu BJ, Chang SK. Journal of food science, 2008, Mar.;73(2):1750-3841.” data-widget=”linkref Cook up some black bean soup or make a healthy black-bean taco.

7. Lentils
Price: $0.12 per 1/2 cup, $1.50 per pound (dry, in bulk) These mild legumes add richness to curries and soups, plus act as a great meat replacement for Bolognese sauce or burgers. Bonus points: Lentils have more protein per pound than beef and are rich with antioxidants, so it might be worth it to trade in that cheeseburger once in a while.Phenolic substance characterization and chemical and cell-based antioxidant activities of 11 lentils grown in the northern United States. Xu B, Chang SK. Journal of agricultural and food chemistry, 2010, Apr.;58(3):1520-5118.” data-widget=”linkref

8. Garbanzo Beans
Price: $0.30 per 1/2 cup, $3 per can These little beans (also known as chickpeas) pack a serious amount of fiber. Roast them with olive oil and your favorite spices (think: cumin, paprika, or curry powder) to use as a crouton replacement or blend into DIY hummus.

9. Tofu
Price: $0.20 per ounce, $3 per pound High in protein and low in fat, tofu is a delicious staple for vegetarians and meat-eaters alike. Plus soy in moderation may help reduce cholesterol and the risk of breast cancer.Vegan proteins may reduce risk of cancer, obesity, and cardiovascular disease by promoting increased glucagon activity. McCarty MF. Medical hypotheses, 2000, Mar.;53(6):0306-9877.” data-widget=”linkref Pan-fry tofu with veggies in your next stir-fry, scramble extra-firm tofu like eggs, and try the silken variety in a fruit smoothie.

10. Pumpkin Seeds
Price: $0.50 per ounce, $6 per pound Pumpkin seeds (also known as pepitas) go well in a salad or can be roasted with spices for a crunchy snack. Seeing as they’re filled with essential vitamins and minerals, along with protein and iron, you really can’t go wrong.Amino acid, mineral and fatty acid content of pumpkin seeds (Cucurbita spp) and Cyperus esculentus nuts in the Republic of Niger. Glew RH, Glew RS, Chuang LT. Plant foods for human nutrition (Dordrecht, Netherlands), 2006, Sep.;61(2):0921-9668.” data-widget=”linkref

11. Oats
Price: $1 per pound (in bulk)Oats are high in fiber, low in fat, and may even help lower cholesterol.Cholesterol-lowering effects of oat ?-glucan. Othman RA, Moghadasian MH, Jones PJ. Nutrition reviews, 2011, Sep.;69(6):1753-4887. Oat: unique among the cereals. Sadiq Butt M, Tahir-Nadeem M, Khan MK. European journal of nutrition, 2008, Feb.;47(2):1436-6207.” data-widget=”linkrefYou probably already know about oatmeal, but don’t be afraid to mix things up with one of these overnight oats recipes instead.

12. Canned Salmon
Price: $0.20 per ounce, $2.50 per 14.75 -ounce can No need to splurge on a salmon fillet to enjoy this omega-3-packed seafood.Fatty fish, marine omega-3 fatty acids and incidence of heart failure. Levitan EB, Wolk A, Mittleman MA. European journal of clinical nutrition, 2010, Mar.;64(6):1476-5640.” data-widget=”linkref Grab the canned version for some protein power—without having to dish out big bucks. Then try whipping up a batch of homemade salmon burgers.

13. Canned Tuna
Price: $0.30 per ounce, $1.50 per 5-ounce can Not only is tuna cheap, but it’s an another easy way to get omega-3’s (which play a crucial role in brain health). Try mixing with hummus or Greek yogurt for a healthier tuna salad.

14. Whey Protein
Price: $0.75 per scoop, $40 per 3-pound container Need an extra dose of protein? Add whey protein to a smoothie or bowl of oatmeal, or sneak it into your next batch of brownies.

Dairy
15. Yogurt
Price: $1 per 6-ounce cup Pick up a breakfast treat that’s filled with protein and calcium. Just beware of flavors loaded with extra sugar. Greek yogurt is also awesome—and full of protein and probiotics—but it can be more expensive than our $1 limit.

16. Cottage Cheese
Price: $1 per 1/2 cup, $5.50 per 16-ounce container This clumpy, mild cheese is surprisingly high in protein, and tastes great in both sweet and savory dishes. Like yogurt, cottage cheese typically comes in full-fat, low-fat, and fat-free varieties, so choose whichever fits best into your diet. Try it topped with sliced pineapple and berries or make it savory in a creamy pasta sauce.

17. Milk
Price: $0.25 per cup, $4 per gallon Add a splash of milk to a fruit smoothie or enjoy it as a classic: over a bowl of cereal. One calcium-filled glass can help keep teeth strong and even stave off excess pounds.High-calcium milk prevents overweight and obesity among postmenopausal women. 

Whole Grains
18. Brown Rice
Price: $0.18 per 1/4 cup, $2 per pound Use instead of white rice in any recipe (just note that cooking times differ) for a more exciting flavor and texture. Plus this whole-grain version of rice is full of fiber and may cut the risk of diabetes.Pitfall in MR imaging of lymphadenopathy after lymphangiography. Buckwalter KA, Ellis JH, Baker DE. Radiology, 1986, Dec.;161(3):0033-8419.” data-widget=”linkref

19. Whole-Wheat Pasta
Price: $0.37 per 1/2 cup, $3 per box Enjoy whole-wheat pasta’s nutty flavor paired with sautéed veggies and a fresh tomato sauce. Not only is the whole-wheat version of pasta more complex in taste, it’s packed with fiber, antioxidants, and protein, and it may even help lower the risk of heart disease.Whole grain intake and cardiovascular disease: a meta-analysis. Mellen PB, Walsh TF, Herrington DM. Nutrition, metabolism, and cardiovascular diseases : NMCD, 2007, Apr.;18(4):1590-3729.” data-widget=”linkref

20. Popcorn
Price: $0.30 per 1/2 cup, $1 per pound for kernels Popcorn is a low-calorie snack that’s also a good source of fiber. Pop kernels on the stove or in a paper bag in the microwave, and then top with your fave spices, like taco seasoning or cinnamon and sugar.

21. Quinoa
Price: $0.60 per 1/4 cup, $5 per 12-ounce box Add cooked quinoa to sweet granola bowls and veg-filled salads or serve as a side instead of pasta. Bursting with protein and fiber, quinoa also contains all nine essential amino acids (that the body can’t produce on its own).Nutritional quality of the protein in quinoa (Chenopodium quinoa, Willd) seeds. Ruales J, Nair BM. Plant foods for human nutrition (Dordrecht, Netherlands), 1992, Apr.;42(1):0921-9668.” data-widget=”linkref

Fruit
22. Grapes
Price: $0.75 per cup, $1.50 per pound Add sliced grapes to salads instead of sugar-filled dried fruit or freeze them for a refreshing summer snack. It’ll be well worth it: These tiny fruits are high in antioxidants that may help reduce cholesterol.

23. Watermelon
Price: $0.30 per cup, $5 per melon This feisty superfood might have Viagra-like effects, but it’s guaranteed to be packed with vitamin C, a cancer-fighting antioxidant that helps strengthen immunity and promote bone health. Slice and enjoy (or make these simple watermelon popsicles).

24. Bananas
Price: $0.50 per banana, $2 per bunch Filled with fiber and potassium, these 100-calorie snacks may even help with a hangover. Enjoy sliced with your favorite nut butter or blend frozen bananas into creamy single-ingredient ice cream.

25. Kiwi
Price: $0.40 per kiwi Did you know kiwis are actually berries? Start snacking, because they’re packed with vitamin C and fiber. Add a kiwi to your next fruit salad or granola bowl, or enjoy straight up with a spoon.

26. Cantaloupe
Price: $0.50 per 1/2 cup, $3 per small melon Cantaloupe makes a perfect spring or summer treat. The antioxidant-packed fruit pairs well with yogurt or can be frozen as a DIY popsicle.

27. Apples
Price: $0.75 per apple An apple a day, right? Studies have shown that eating apples may be linked to a decreased risk of cancer, diabetes, and even asthma. Eat them plain, smear with a nut butter, or pair with a few cubes of cheese for a protein-and-carb rich snack—perfect post workout.

28. Pears
Price: $0.85 each, $1.75 per pound White fruits, like white pears, may help prevent strokes, at least according to one study.Colors of fruit and vegetables and 10-year incidence of stroke. Oude Griep LM, Verschuren WM, Kromhout D. Stroke; a journal of cerebral circulation, 2011, Sep.;42(11):1524-4628.” data-widget=”linkref But that doesn’t mean you should only stick to one type. Keep your diet diverse and try the Bartlett, Bosc, and Anjou varieties.

29. Oranges
Price: $0.50 each, $1 per pound Oranges might get talked about for their vitamin C content, but they’re also strong in fiber, folate, and potassium. If you’d rather go with the juice, skip the carton and squeeze your own to make sure you’re not downing any unnecessary sugar.

Vegetables
30. Garlic
Price: $0.30 per bulb Add minced garlic to any pan of sautéed vegetables or roast whole in the oven for a sweeter flavor, and then blend into salad dressings and dips. In addition to its vitamins and minerals, garlic may help enhance memory (at least in rats) and reduce the chance of heart attack.Repeated administration of fresh garlic increases memory retention in rats. Haider S, Naz N, Khaliq S. Journal of medicinal food, 2009, Feb.;11(4):1557-7600. Allium vegetable intake and risk of acute myocardial infarction in Italy. Galeone C, Tavani A, Pelucchi C. European journal of nutrition, 2009, Jan.;48(2):1436-6215.” data-widget=”linkref

31. Canned Pumpkin
Price: $0.75 per 1/2 cup, about $2.50 per 15-ounce can Pumpkin’s orange color comes from carotenoids, a plant pigment with powerful antioxidant properties.Carotenoid composition and vitamin A value of a squash and a pumpkin from northeastern Brazil. Arima HK, Rodríguez-Amaya DB. Archivos latinoamericanos de nutricio?n, 1992, Mar.;40(2):0004-0622.” data-widget=”linkref Add canned pumpkin to sweet or savory recipes—smoothies, muffins, veggie burgers, curries, and more!

32. Canned Tomatoes
Price: $0.50 per 1/2 cup, $1.80 per 14.8-ounce can Tomatoes retain exceptional amounts of the antioxidant lycopene even after cooking and canning.Lycopene, tomatoes, and the prevention of coronary heart disease. Rao AV. Experimental biology and medicine (Maywood, N.J.), 2002, Dec.;227(10):1535-3702.” data-widget=”linkref Canned tomatoes are perfect for homemade sauces and stews, but be on the lookout for cans with no added sodium or sugar (and that are preferably BPA-free).

33. Onions
Price: $0.18 each, $0.59 per pound Use along with garlic as an aromatic base for stir-fries, stews, and sauces; or sauté until golden and sweet, then add to salads, pastas, or sandwiches. Not only will your food be more flavorful, but you’ll also be doing your body a favor—onions pack a surprisingly nutritious punch, including a hefty dose of antioxidants.Onions–a global benefit to health. Griffiths G, Trueman L, Crowther T. Phytotherapy research : PTR, 2003, Mar.;16(7):0951-418X.” data-widget=”linkref

34. Carrots
Price: $0.50 each, $2 per pound Raw carrot sticks are perfect for dipping into hummus or nut butters (don’t knock it ’til you try it!) and taste great roasted with other root veggies and a drizzle of olive oil. That nutritious crunch comes with tons of vitamin A.Spinach or carrots can supply significant amounts of vitamin A as assessed by feeding with intrinsically deuterated vegetables. Tang G, Qin J, Dolnikowski GG. The American journal of clinical nutrition, 2005, Nov.;82(4):0002-9165.” data-widget=”linkref

35. Winter Squash
Price: $0.50 per 1/2 cup, $1.50 per pound Squash is a versatile veggie filled with vitamins, fiber, and potassium. Roast a squash and fill with whole grains, like brown rice or quinoa and veggies. Top with Greek yogurt or part-skim ricotta for a hearty vegetarian dinner—no bowls needed.

36. Kale
Price: $0.50 per cup (raw, chopped), $2 per bunchKale is the antioxidant king among fruits and veggies and contains vitamins A, C, and K; fiber; calcium; iron; and potassium. Bonus: kale chips. Need we say more?

37. Beets
Price: $0.35 each, $1 per pound These magenta gems are filled with betalains, an antioxidant that may help prevent cancer and other degenerative diseases.Chemoprevention of DMBA-induced UV-B promoted, NOR-1-induced TPA promoted skin carcinogenesis, and DEN-induced phenobarbital promoted liver tumors in mice by extract of beetroot. Kapadia GJ, Azuine MA, Sridhar R. Pharmacological research, 2003, Sep.;47(2):1043-6618. Betalains–a new class of dietary cationized antioxidants. Kanner J, Harel S, Granit R. Journal of agricultural and food chemistry, 2002, Jan.;49(11):0021-8561.” data-widget=”linkref They are also packed with folate, fiber, and vitamins galore—making them one of the best health bargains around.Nutritional and functional potential of Beta vulgaris cicla and rubra. Ninfali P, Angelino D. Fitoterapia, 2013, Jun.;89():1873-6971.” data-widget=”linkref Roast with olive oil for salads or as a side dish, or add to a smoothie.

38. Broccoli
rice: $0.50 per 1/2 cup, $2 per bunchBroccoli has remarkably high levels of folate and vitamin C, which may help reduce the risk of certain cancers and heart disease.Effects of different cooking methods on health-promoting compounds of broccoli. Yuan GF, Sun B, Yuan J. Journal of Zhejiang University. Science. B, 2009, Oct.;10(8):1862-1783. Epidemiological studies on brassica vegetables and cancer risk. Verhoeven DT, Goldbohm RA, van Poppel G. Cancer epidemiology, biomarkers & prevention : a publication of the American Association for Cancer Research, cosponsored by the American Society of Preventive Oncology, 1997, Jan.;5(9):1055-9965.” data-widget=”linkref This veggie tastes amazing blended in soup, stuffed in potatoes, tossed in frittatas, or simply cooked with a bit of garlic and olive oil.

39. Spinach
Price: $0.50 per cup, $2 per bunch Replace lettuce with spinach in salads for added benefits or add a few handfuls into your morning smoothie. These unassuming greens are nutrient-dense with vitamins A, K, and calcium.

40. Sweet Potatoes
Price: $0.50 each, $1 per pound Try this healthy alternative in place of a bread slice the next time you’re whipping up an avocado-on-toast recipe. Sweet potatoes have high levels of vitamin A and calcium, plus they’re lower in carbohydrates than their white counterparts (just in case you’re counting). Studies also show the root veggie has anti-cancer, anti-inflammatory, and anti-diabetic activities.Sweet potato (Ipomoea batatas [L.] Lam)–a valuable medicinal food: a review. Mohanraj R, Sivasankar S. Journal of medicinal food, 2014, Jun.;17(7):1557-7600.” data-widget=”linkref

41. Edamame
Price: $0.50 per 1/2 cup, $3 per 10-ounce frozen packageSkip the chips and enjoy edamame steamed with a touch of salt. These bite-size legumes are filled with fiber and protein, which make for a great afternoon snack.', 48, '2020-04-22 23:27:00', 1);
INSERT INTO healthy_food.posts (post_id, visibility_id, creator_id, title, description, media_id, timestamp, enabled) VALUES (33, 2, 14, 'HEALTHY APPLE SALAD WITH RAISINS AND WALNUTS', 'INGREDIENTS
For The Dressing:

½ cup Greek yogurt
2 tablespoons mayonnaise
3 tablespoons lemon juice, freshly squeezed
¾ teaspoon kosher salt
¼ teaspoon black pepper
For The Salad:

4 cups baby spinach, rinsed and spin dried
2 stalks celery, chopped
¼ cup red onion, thinly sliced
¼ cup raisins
½ cup walnuts (or pecans), roughly chopped
1 apple, cored and thinly sliced
? cup cheddar, shredded
INSTRUCTIONS
To make the dressing: place yogurt, mayonnaise, lemon juice, and salt and pepper in a bowl. Give it a whisk. Set aside.
To make the salad: Place spinach, celery, red onion, raisins, walnuts, and apple slices in a large salad bowl.
Drizzle it with the dressing and give it a gentle toss.
When ready to serve, top it off with shredded cheddar.

NOTES
Note: I added a handful of pomegranate seeds at the last minute mostly for a pop of color for pretty pictures. However, it is totally optional. ', 49, '2020-04-22 23:36:28', 1);