INSERT INTO healthy_food.authorities (username, authority) VALUES ('anor@abv.bg', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('ca1234km@abv.bg', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('g.kabadaev@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('georgi.kabadaev@abv.bg', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('healthy.food.social.network@gmail.com', 'ROLE_ADMIN');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('healthy.food.social.network@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('ilchavdarov@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('ivan.petrov.testov@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('mariana_marinova.dr@abv.bg', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('silviatdivanova@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('tanya.serdon@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('tkabadaeva@bcserdon.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('tkabadaeva@fineline.bg', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('tkabadaeva@gmail.com', 'ROLE_USER');
INSERT INTO healthy_food.authorities (username, authority) VALUES ('ykabadaeva@gmail.com', 'ROLE_USER');