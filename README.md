# Healthy Food Social Network [link](https://healthy-food-social-network.herokuapp.com/)

## by Silviya and Tanya
Application provides functionality based on the role of the user that is using it. There are three types of users: anonymous, regular and administrator.

# Project Description
Healthy Food Social Network is a web application that enables you to: 
* Create user account
* Connect with people 
* Create, comment and like posts 
* Get a feed of the public posts and posts of your connections. 

Anonymous users can browse all public posts and see their details. They can also filter them by title, category and sort them by date, likes and comments. Anoynymous users can browse all user profiles and see public information.

Registered users can create, comment and like posts and comments. They can also manage their own posts, comments and personal information as well. Registered users can connect with others and see all their posts and personal information if the connection is confirmed.

Administrators have full control over all posts, comments and users. They have full access to the system and permissions to administer all entities: users, posts, comments, categories and nationalities.

## Rest API documentation [link](https://healthy-food-social-network.herokuapp.com/swagger-ui.html)

## How we work together can be found here [link](https://trello.com/b/J34UutYR/social-network)

# Technology:
- *JDK version 1.8*
- *SpringMVC and SpringBoot framework*
- *Spring Security*
- *Spring Data JPA*
- *IntelliJ IDEA*
- *Thymeleaf*
- *AJAX*
- *MariaDB*
- *Swagger*
- *Bootstrap Template*
- *Cloudinary*
- *CI/CD*
  
# Database diagram: 
![database diagram](./database/Diagram.jpg)

# UI

## Login 
![login](./images/login.png)

## Home 
![hone](./images/home.png)

## Admin 
![admin](./images/admin.png)

## Latest posts 
![latest-posts](./images/latest-posts.png)

## Profile 
![profile-connect](./images/profile-connect.png)

## Users 
![users](./images/users.png)

## Requests 
![requests](./images/requests.png)